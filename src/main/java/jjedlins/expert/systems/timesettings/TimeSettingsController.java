package jjedlins.expert.systems.timesettings;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import jjedlins.expert.systems.common.UserData;
import jjedlins.expert.systems.screen.BaseScreenController;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.util.ResourceBundle;

@Component
public class TimeSettingsController extends BaseScreenController implements Initializable {


	@FXML
	public ChoiceBox<Integer> trainingDaysNumber;
	@FXML public ChoiceBox<TimeOfDay> trainingTimeChoiceBox;
	@FXML
	public ToggleGroup trainingDaysToggle = new ToggleGroup();
	@FXML public Slider timeForCooking;
	public Slider moneySlider;
	public Slider mealsNumber;
	public Button next;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		trainingDaysNumber.getItems().setAll(2, 3, 4, 5, 6, 7);
		trainingDaysNumber.getSelectionModel().select(1);

		trainingTimeChoiceBox.getItems().setAll(TimeOfDay.values());
		trainingTimeChoiceBox.getSelectionModel().select(0);

		timeForCooking.valueProperty().addListener((observable, oldValue, newValue) -> timeForCookingSet(newValue));

		trainingDaysNumber.selectionModelProperty().addListener((observable, oldValue, newValue) -> {
			Integer number = Integer.parseInt(newValue.toString());

			UserData.userData.setTrainingsPerWeek(number);
		});

		trainingTimeChoiceBox.selectionModelProperty().addListener((observable, oldValue, newValue) -> {
			TimeOfDay timeOfDay = TimeOfDay.valueOf(newValue.toString());

			UserData.userData.setTimeOfDay(timeOfDay);
		});

		timeForCooking.valueProperty().addListener((observable, oldValue, newValue) -> UserData.userData.setTimeForCooking(newValue.intValue()));

		moneySlider.valueProperty().addListener((observable, oldValue, newValue) -> UserData.userData.setMoney(newValue.intValue()));

		mealsNumber.valueProperty().addListener((observable, oldValue, newValue) -> {
			UserData.userData.setNumberOfMeals(newValue.intValue());
		});

		next.setOnMouseClicked(event -> this.sc.loadScreen("/fxml/result.fxml"));

	}

	@FXML
	private void timeForCookingSet(Number newValue) {

		System.out.println(newValue);
	}

	public void selectedDay(ActionEvent actionEvent) {

		CheckBox source = (CheckBox) actionEvent.getSource();
		Integer selectDay = Integer.parseInt(source.getUserData().toString());

		if (source.isSelected()) {
			UserData.userData.getDaysOfTraining().add(selectDay);
		}else {
			UserData.userData.getDaysOfTraining().remove(selectDay);

		}

		UserData.userData.getDaysOfTraining().sort(Integer::compareTo);
	}
}
