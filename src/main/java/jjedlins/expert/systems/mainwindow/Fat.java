package jjedlins.expert.systems.mainwindow;

public enum Fat {

	FAT_5,
	FAT_10,
	FAT_15,
	FAT_20,
	FAT_25,
	FAT_30,
	FAT_35,
	FAT_40,
	FAT_45,
	FAT_50
}
