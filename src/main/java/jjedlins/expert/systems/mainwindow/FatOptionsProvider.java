package jjedlins.expert.systems.mainwindow;

import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import jjedlins.expert.systems.user.Sex;
import org.springframework.stereotype.Service;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.io.Files.getNameWithoutExtension;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.Comparator.comparing;
import static javafx.scene.control.ContentDisplay.TOP;

@Service
public class FatOptionsProvider {

	List<ToggleButton> getFatOptions(ToggleGroup fatToggle, Sex sex) {

		File directory = null;
		try {
			directory = new File(getClass().getClassLoader().getResource("img/fat/" + sex.name().toLowerCase() + "/").toURI());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		if (directory == null) {
			return emptyList();
		}

		File[] files = directory.listFiles();

		if (files == null) {
			return emptyList();
		}

		return stream(files)
				.sorted(comparing(file -> Integer.valueOf(getNameWithoutExtension(file.getName()))))
				.map(this::createButton)
				.peek(button -> button.setToggleGroup(fatToggle))
				.collect(Collectors.toList());

	}

	private ToggleButton createButton(File file) {

		String fatLevel = getNameWithoutExtension(file.getName());

		ToggleButton button = new ToggleButton(fatLevel + "%");
		button.setGraphic(imageView(file.getPath()));
		button.setUserData("FAT_"+fatLevel);
		button.setContentDisplay(TOP);

		return button;
	}

	private ImageView imageView(String path) {

		try {
			return new ImageView(new Image(Paths.get(path).toUri().toURL().toString()));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return null;
	}
}
