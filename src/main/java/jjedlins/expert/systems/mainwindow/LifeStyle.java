package jjedlins.expert.systems.mainwindow;

public enum LifeStyle {

	HOUSEKEEPER,
	ACTIVE_WORKER,
	SPORTSMAN,
	LOW_ACTIVITY
}
