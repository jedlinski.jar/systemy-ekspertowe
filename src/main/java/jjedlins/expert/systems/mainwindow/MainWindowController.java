package jjedlins.expert.systems.mainwindow;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;
import jjedlins.expert.systems.common.UserData;
import jjedlins.expert.systems.screen.BaseScreenController;
import jjedlins.expert.systems.user.Person;
import jjedlins.expert.systems.user.Sex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.ResourceBundle;

@Component
public class MainWindowController extends BaseScreenController implements Initializable {

	@FXML
	public HBox dataBox;
	public TextField heightField;
	public TextField weightField;
	@FXML
	private HBox fatBox;
	@FXML
	private ToggleGroup personSex = new ToggleGroup();
	@FXML
	private ToggleGroup fatToggle = new ToggleGroup();
	@FXML
	private ToggleGroup lifeStyleToggle = new ToggleGroup();
	@FXML
	private HBox lifestyle = new HBox();
	@FXML
	private Label fatLabel = new Label();
	@FXML
	private TextFlow fatText = new TextFlow();
	@FXML
	private VBox aimBox = new VBox();
	@FXML
	private HBox aimOptions = new HBox();
	@FXML
	private ToggleGroup aimToggle = new ToggleGroup();
	@FXML
	private TextField fatField = new TextField();

	@Autowired
	private FatOptionsProvider fatOptionsProvider;
	@Autowired
	private LifeStyleProvider lifeStyleProvider;
	@Autowired
	private AimOptionsProvider aimOptionsProvider;


	@Override
	public void initialize(URL location, ResourceBundle resources) {

		personSex.selectedToggleProperty().addListener((list, oldToggle, newToggle) -> {

			Sex newSex = Sex.valueOf((String) newToggle.getUserData());
			fatBox.getChildren().setAll(fatOptionsProvider.getFatOptions(fatToggle, newSex));
			fatLabel.setVisible(true);
			fatText.setVisible(true);

			aimOptions.getChildren().setAll(aimOptionsProvider.getAimOptions(aimToggle, newSex));

			Person.getInstance().setSex(newSex);
		});

		fatToggle.selectedToggleProperty().addListener((list, oldToggle, newToggle) -> {

			if (newToggle == null) {
				fatToggle.selectToggle(oldToggle);
				newToggle = oldToggle;
			}

			fatField.setText(((ToggleButton) newToggle).getText());

			Sex sex = Person.getInstance().getSex();
			aimOptions.getChildren().setAll(aimOptionsProvider.getAimOptions(aimToggle, sex));
			aimBox.setVisible(true);
		});

		lifestyle.getChildren().setAll(lifeStyleProvider.getLifestyleOptions(lifeStyleToggle));

		heightField.textProperty().addListener((observable, oldValue, newValue) -> {
			try {
				int height = Integer.parseInt(newValue);
				UserData.userData.setHeight(height);
			} catch (Exception e) {
				;
			}
		});

		weightField.textProperty().addListener((observable, oldValue, newValue) -> {
			try {
				int weight = Integer.parseInt(newValue);
				UserData.userData.setWeight(weight);
			} catch (Exception e) {
				;
			}
		});

		personSex.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
			if (personSex.getSelectedToggle() != null) {

				String sex = personSex.getSelectedToggle().getUserData().toString();

				UserData.userData.setSex(Sex.valueOf(sex));
			}
		});

		lifeStyleToggle.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
			if (new_toggle != null) {

				LifeStyle lifeStyle = LifeStyle.valueOf(new_toggle.getUserData().toString());

				UserData.userData.setLifeStyle(lifeStyle);
			}
		});

		fatToggle.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {

				int fat = Integer.valueOf(newValue.getUserData().toString().replace("FAT_", ""));

				UserData.userData.setCurrentFat(fat);
			}
		});

		aimToggle.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null) {

				Aim aim = Aim.valueOf(newValue.getUserData().toString().toUpperCase());

				UserData.userData.setAim(aim);
			}
		});
	}

	@FXML
	private void handleTimeSettingsViewInit(MouseEvent event) {


		this.sc.loadScreen("/fxml/time_settings.fxml");
	}
}