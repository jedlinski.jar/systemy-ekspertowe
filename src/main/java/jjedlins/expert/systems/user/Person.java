package jjedlins.expert.systems.user;

public final class Person {

	private static final Person INSTANCE = new Person();

	private Sex sex;

	private Person(){}

	public static Person getInstance() {
		return INSTANCE;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}
}
