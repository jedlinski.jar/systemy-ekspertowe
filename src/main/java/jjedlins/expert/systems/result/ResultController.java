package jjedlins.expert.systems.result;

import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import jjedlins.expert.systems.screen.BaseScreenController;
import jjedlins.expert.systems.timesettings.TimeOfDay;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

import static jjedlins.expert.systems.common.UserData.userData;

@Component
public class ResultController extends BaseScreenController implements Initializable {

	public VBox vbox;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		textWithText("Twoje dzienne zapotrzebowanie kaloryczne: " + (int) CaloriesDailyNeededCalculator.getCaloriesNeeded() + "kcal");
		dietInfo();
		textWithText("");
		trainingInfo();
	}

	private void trainingInfo() {

		TableView<TableData> table = new TableView<>();

		TableColumn<TableData, String> productCol = new TableColumn<>("Produkt");
		productCol.setCellValueFactory(
				new PropertyValueFactory<>("product")
		);
		TableColumn<TableData, Object> firstMeal = new TableColumn<>("Po przebudzeniu");
		firstMeal.setCellValueFactory(
				new PropertyValueFactory<>("firstMeal")
		);
		TableColumn<TableData, Object> beforeSleepCol = new TableColumn<>("Przed snem");
		beforeSleepCol.setCellValueFactory(
				new PropertyValueFactory<>("beforeSleep")
		);
		TableColumn<TableData, Object> beforeTraining = new TableColumn<>("Przed treningiem");
		beforeTraining.setCellValueFactory(
				new PropertyValueFactory<>("beforeTraining")
		);
		TableColumn<TableData, Object> afterTraining = new TableColumn<>("Po treningu");
		afterTraining.setCellValueFactory(
				new PropertyValueFactory<>("afterTraining")
		);

		table.getColumns().addAll(productCol, firstMeal, beforeSleepCol, beforeTraining, afterTraining);

		table.setItems(TableDataGenerator.getTableData());

		vbox.getChildren().add(table);

	}


	private void textWithText(String content) {

		Text text = new Text(content);
		text.setFont(new Font(18));
		text.setWrappingWidth(1200);

		vbox.getChildren().add(text);
	}

	private void dietInfo() {

		int bialko = (int) (userData.getWeight() * 2);
		int kalorieBialka = bialko * 4;

		int kalorieTluszcz = (int) (CaloriesDailyNeededCalculator.getCaloriesNeeded() * 0.3);
		int tluszcz = (int)kalorieTluszcz / 9;

		int kalorieWegle = (int) (CaloriesDailyNeededCalculator.getCaloriesNeeded() - kalorieBialka - kalorieTluszcz);
		int wegle = (int) kalorieWegle / 4;

		String infoString = "Białko: " + bialko + "g (" + kalorieBialka + "kcal)\n" +
				"Tłuszcz: " + tluszcz + "g (" + kalorieTluszcz + "kcal)\n" +
				"Węglowodany: " + wegle + "g (" + kalorieWegle + "kcal)";

		if (CaloriesDailyNeededCalculator.needsFatBurn()) {
			textWithText("Dieta redukcyjna: \n" + infoString);
		}
		else {
			textWithText("Dieta na masę: \n" + infoString);
		}

		textWithText("");
		textWithText("Kaloryczny podział posiłków;");

		int mealsNum = userData.getNumberOfMeals();

		int bialkoPerMeal = bialko / mealsNum;
		int tluszczPerMeal = tluszcz / mealsNum;
		int weglePerMeal = wegle / (mealsNum - 1);

		int firstBialko = bialkoPerMeal - (userData.getTimeOfDay() == TimeOfDay.RANO ? 6 : 0);
		int firstTluszcz = tluszczPerMeal - (userData.getTimeOfDay() == TimeOfDay.RANO ? 6 : 0);

		bialkoPerMeal += userData.getTimeOfDay() == TimeOfDay.RANO ? 2 : 0;
		tluszczPerMeal += userData.getTimeOfDay() == TimeOfDay.RANO ? 2 : 0;

		textWithText("Pierwszy posiłek (BT): " + firstBialko + "g białka i " + firstTluszcz +"g tłuszczu");
		textWithText("Pozostałe " + (mealsNum - 1) + " posiłki (BTW): " + bialkoPerMeal + "g białka , " + tluszczPerMeal +"g tłuszczu i " + weglePerMeal + "g węglowodanów");

		int money = userData.getMoney();

		List<String> fats = new ArrayList<>(Arrays.asList("Olej kokosow", "orzechy nerkowca", "smalec", "tłuste mięsa", "oliwa z oliwek"));
		List<String> bialka = new ArrayList<>(Arrays.asList("Mięso", "prawdziwe nabiały", "odżywki białkowe"));
		List<String> wegles = new ArrayList<>(Arrays.asList("ryż (biały, basmanti, jaśminowy, parboiled)", "wafle ryżowe", "ziemniaki", "owoce i warzywa", "gryka", "proso"));


		if (money >= 750) {
			fats.addAll(Arrays.asList("masło", "orzechy brazylijskie", "pistacje", "olej lniany"));
			bialka.addAll(Arrays.asList("jaja", "gryka"));
			wegles.addAll(Arrays.asList("komosa ryżowa", "amarantus"));
		}
		if (money >= 1250) {
			fats.addAll(Arrays.asList("masło klarowane, oliwa z oliwek (w szczególności liguryjskich)", "ryby"));
			bialka.addAll(Arrays.asList("ryby (i owoce morza)", "jagody goji", "amarantus"));
			wegles.addAll(Arrays.asList("bataty", "maniok", "tapioka"));
		}

		Collections.shuffle(fats);
		Collections.shuffle(bialka);
		Collections.shuffle(wegles);

		textWithText("");
		textWithText("Zalecane źródła białka:");
		textWithText(bialka.stream().collect(Collectors.joining(", ")));

		textWithText("Zalecane źródła tłuszczu:");
		textWithText(fats.stream().collect(Collectors.joining(", ")));

		textWithText("Zalecane źródła węglowodanów:");
		textWithText(wegles.stream().collect(Collectors.joining(", ")));
	}
}
