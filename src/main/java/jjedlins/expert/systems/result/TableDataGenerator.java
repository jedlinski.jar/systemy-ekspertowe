package jjedlins.expert.systems.result;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jjedlins.expert.systems.common.UserData;
import jjedlins.expert.systems.mainwindow.Aim;
import jjedlins.expert.systems.mainwindow.LifeStyle;
import jjedlins.expert.systems.user.Sex;

import java.util.Random;

import static jjedlins.expert.systems.common.UserData.userData;

public class TableDataGenerator {

	public static ObservableList<TableData> getTableData() {

		return FXCollections.observableArrayList(
			getProtein(),
				getCreatine(),
				getAshwagandha(),
				getCatsClas(),
				getMg(),
				getOmega3(),
				getWitD(),
				getWitB(),
				getWitC()
		);
	}

	private static TableData getWitC() {

		return new TableData("Wit. C", "1g", new Random().nextInt(2) + 1 + "g", "", "");
	}

	private static TableData getWitB() {

		int dawka = 1;

		if (userData.getAim() == Aim.KULTURYSTA || userData.getAim() == Aim.SPORTOWIEC) {
			dawka = 2;
		}

		return new TableData("Wit. B (P-5-P)", dawka + " tab", "", "", "");

	}

	private static TableData getWitD() {

		int dawka = 2;

		if (userData.getSex() == Sex.FEMALE) {
			if (userData.getAim() == Aim.WYSPORTOWANA) {
				dawka = 3;
			}
		}

		if (userData.getSex() == Sex.MALE) {

			dawka = 3;

			if (userData.getAim() == Aim.KULTURYSTA || userData.getAim() == Aim.SIŁACZ_NA_MASIE) {
				dawka = 4;
			}
		}

		return new TableData("Wit. D3+K2", dawka + "-" + (dawka + 1) + "tyś. j.", "", "", "");
	}

	private static TableData getOmega3() {

		int dawka = 2;

		if (userData.getSex() == Sex.MALE) {
			dawka = 3;
		}

		return new TableData("OMEGA 3", dawka + " tab", "", "", "");
	}

	private static TableData getMg() {
		return new TableData("Magnez", "", "400mg", "", "");
	}

	private static TableData getCatsClas() {

		String dawka = "500mg";

		if ((userData.getCurrentFat() > 25 && userData.getSex() == Sex.MALE) || userData.getCurrentFat() > 30 && userData.getSex() == Sex.FEMALE) {
			dawka = "1g";
		}

		return new TableData("Cat's Claw", "500mg", dawka, "", "");
	}

	private static TableData getAshwagandha() {

		LifeStyle lifeStyle = userData.getLifeStyle();
		int dawka = 500;
		if (lifeStyle == LifeStyle.ACTIVE_WORKER || lifeStyle == LifeStyle.SPORTSMAN) {
			dawka = 1000;
		}

		return new TableData("Ashwagandha", "", dawka + "mg", "", "");
	}

	private static TableData getCreatine() {

		int dawka = 0;

		if (userData.getSex() == Sex.MALE) {
			Aim aim = userData.getAim();

			if (aim == Aim.SPORTOWIEC) {
				dawka = 7;
			}

			if (aim == Aim.SIŁACZ_NA_MASIE) {
				dawka = 8;
			}

			if (aim == Aim.KULTURYSTA) {
				dawka = 10;
			}

			if (aim == Aim.CHUDY_MODEL) {
				dawka = 5;
			}

			if (aim == Aim.MODEL_FITNESS) {
				dawka = 8;
			}
		}

		if (userData.getSex() == Sex.FEMALE) {
			Aim aim = userData.getAim();

			if (aim == Aim.FITNESS_SYLWETKOWY) {
				dawka = 6;
			}

			if (aim == Aim.WYSPORTOWANA) {
				dawka = 7;
			}

			if (aim == Aim.BIKINI_FITNESS) {
				dawka = 0;
			}

			if (aim == Aim.CHUDA_MODELKA) {
				dawka = 0;
			}

		}

		return new TableData("Kreatyna", "", "", "" + dawka + "g", dawka + "g");
	}

	private static TableData getProtein() {


		return new TableData("Białko serwatkowe (izolat)", "30g (w dzień treningowy)", "", "", "30g + dojrzały banan");
	}
}
