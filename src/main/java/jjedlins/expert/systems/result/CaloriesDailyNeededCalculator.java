package jjedlins.expert.systems.result;

import jjedlins.expert.systems.common.UserData;
import jjedlins.expert.systems.mainwindow.Aim;
import jjedlins.expert.systems.mainwindow.LifeStyle;
import jjedlins.expert.systems.user.Sex;

import static jjedlins.expert.systems.common.UserData.userData;

public class CaloriesDailyNeededCalculator {

	public static double getCaloriesNeeded() {

		double bmr = 9.99 * userData.getWeight() + 6.25 * userData.getHeight();

		if (userData.getSex() == Sex.MALE) {
			bmr += 5;
		}

		if (userData.getSex() == Sex.FEMALE) {
			bmr -= 161;
		}

		double tea = (getTeaParam() * 60 * userData.getTrainingsPerWeek()) / 7;

		double epoc = 35;

		double neat = getNeat();

		double tdee = bmr + tea + neat;

		return tdee + 0.1 * tdee + (userData.getSex() == Sex.MALE ? 300 : 150);
	}

	public static double getTeaParam() {

		if (userData.getSex() == Sex.MALE) {

			int currentFat = userData.getCurrentFat();
			Aim aim = userData.getAim();

			if (aim == Aim.MODEL_FITNESS) {

				if (currentFat > 10 && currentFat < 25)
					return 9;

				if (currentFat <= 10) {
					return 10;
				}

				if (currentFat >= 25) {
					return 8;
				}
			}

			if (aim == Aim.SPORTOWIEC) {
				return 8;
			}

			if (aim == Aim.KULTURYSTA) {
				return 9;
			}

			if (aim == Aim.SIŁACZ_NA_MASIE) {
				return 10;
			}

			return 7;

		}

		if (userData.getSex() == Sex.FEMALE) {

			int currentFat = userData.getCurrentFat();
			Aim aim = userData.getAim();

			if (aim == Aim.FITNESS_SYLWETKOWY) {
				if (currentFat >= 15 && currentFat < 25) {
					return 9;
				}

				if (currentFat >= 25) {
					return 7;
				}

				if (currentFat < 15) {
					return 8;
				}
			}

			if (aim == Aim.BIKINI_FITNESS) {

				return 8;
			}

			if (aim == Aim.CHUDA_MODELKA) {
				return 9;
			}

			if (aim == Aim.PLASKI_BRZUCH_I_KRAGLOSCI) {
				return 7;
			}
		}


		return 7;
	}

	public static boolean needsFatBurn() {

		int currentFat = userData.getCurrentFat();
		Aim aim = userData.getAim();

		if (userData.getSex() == Sex.MALE) {

			if (aim == Aim.CHUDY_MODEL || aim == Aim.KULTURYSTA || aim == Aim.SPORTOWIEC) {
				return currentFat >= 10;
			}

			return currentFat >= 20;
		}

		if (userData.getSex() == Sex.FEMALE) {

			if (aim == Aim.CHUDA_MODELKA || aim == Aim.FITNESS_SYLWETKOWY) {

				return currentFat >= 15;
			}

			return currentFat >= 25;
		}

		return false;
	}

	private static double getNeat() {

		double neat = 0;
		LifeStyle lifeStyle = userData.getLifeStyle();

		if (lifeStyle == LifeStyle.LOW_ACTIVITY) {
			neat = 350;
		}

		if (lifeStyle == LifeStyle.HOUSEKEEPER) {
			neat = 500;
		}

		if (lifeStyle == LifeStyle.ACTIVE_WORKER) {
			neat = 750;
		}

		if (lifeStyle == LifeStyle.SPORTSMAN) {
			neat = 900;
		}

		if (userData.getSex() != Sex.MALE) {
			neat -= 100;
		}

		return neat;
	}
}
