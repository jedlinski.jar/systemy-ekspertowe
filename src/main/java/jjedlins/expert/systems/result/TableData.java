package jjedlins.expert.systems.result;

import javafx.beans.property.SimpleStringProperty;

public class TableData {

	private SimpleStringProperty product;
	private SimpleStringProperty firstMeal;
	private SimpleStringProperty beforeSleep;
	private SimpleStringProperty beforeTraining;
	private SimpleStringProperty afterTraining;

	public TableData(String product, String firstMeal, String beforeSleep, String beforeTraining, String afterTraining) {
		this.product =        new SimpleStringProperty(product);
		this.firstMeal =      new SimpleStringProperty(firstMeal);
		this.beforeSleep =    new SimpleStringProperty(beforeSleep);
		this.beforeTraining = new SimpleStringProperty(beforeTraining);
		this.afterTraining =  new SimpleStringProperty(afterTraining);
	}

	public String getProduct() {
		return product.get();
	}

	public SimpleStringProperty productProperty() {
		return product;
	}

	public String getFirstMeal() {
		return firstMeal.get();
	}

	public SimpleStringProperty firstMealProperty() {
		return firstMeal;
	}

	public String getBeforeSleep() {
		return beforeSleep.get();
	}

	public SimpleStringProperty beforeSleepProperty() {
		return beforeSleep;
	}

	public String getBeforeTraining() {
		return beforeTraining.get();
	}

	public SimpleStringProperty beforeTrainingProperty() {
		return beforeTraining;
	}

	public String getAfterTraining() {
		return afterTraining.get();
	}

	public SimpleStringProperty afterTrainingProperty() {
		return afterTraining;
	}
}
