package jjedlins.expert.systems.common;

import jjedlins.expert.systems.mainwindow.Aim;
import jjedlins.expert.systems.mainwindow.LifeStyle;
import jjedlins.expert.systems.timesettings.TimeOfDay;
import jjedlins.expert.systems.user.Sex;

import java.util.ArrayList;
import java.util.List;

public class UserData {

	public static UserData userData = new UserData();

	private int height;
	private int weight;
	private Sex sex;
	private int currentFat;
	private Aim aim;
	private LifeStyle lifeStyle;
	private int trainingsPerWeek;
	private List<Integer> daysOfTraining = new ArrayList<>();
	private TimeOfDay timeOfDay;
	private int timeForCooking;
	private int money;
	private int numberOfMeals;

	public int getTrainingsPerWeek() {
		return trainingsPerWeek;
	}

	public void setTrainingsPerWeek(int trainingsPerWeek) {
		this.trainingsPerWeek = trainingsPerWeek;
	}

	public List<Integer> getDaysOfTraining() {
		return daysOfTraining;
	}

	public void setDaysOfTraining(List<Integer> daysOfTraining) {
		this.daysOfTraining = daysOfTraining;
	}

	public TimeOfDay getTimeOfDay() {
		return timeOfDay;
	}

	public void setTimeOfDay(TimeOfDay timeOfDay) {
		this.timeOfDay = timeOfDay;
	}

	public int getTimeForCooking() {
		return timeForCooking;
	}

	public void setTimeForCooking(int timeForCooking) {
		this.timeForCooking = timeForCooking;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public int getNumberOfMeals() {
		return numberOfMeals;
	}

	public void setNumberOfMeals(int numberOfMeals) {
		this.numberOfMeals = numberOfMeals;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public int getCurrentFat() {
		return currentFat;
	}

	public void setCurrentFat(int currentFat) {
		this.currentFat = currentFat;
	}

	public Aim getAim() {
		return aim;
	}

	public void setAim(Aim aim) {
		this.aim = aim;
	}

	public LifeStyle getLifeStyle() {
		return lifeStyle;
	}

	public void setLifeStyle(LifeStyle lifeStyle) {
		this.lifeStyle = lifeStyle;
	}
}
