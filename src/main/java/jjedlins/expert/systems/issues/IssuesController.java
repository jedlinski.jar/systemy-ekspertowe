package jjedlins.expert.systems.issues;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import jjedlins.expert.systems.screen.BaseScreenController;
import org.springframework.stereotype.Component;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

import static javafx.geometry.HPos.CENTER;

@Component
public class IssuesController extends BaseScreenController implements Initializable {

	public static final Insets MARING = new Insets(15);
	@FXML public GridPane issuesGridPane;
	@FXML public  Button addIssueButton;


	@Override
	public void initialize(URL location, ResourceBundle resources) {

		addIssuesRow();

		/*addIssuesRow();
		addIssuesRow();
		addIssuesRow();
		addIssuesRow();*/
	}

	@FXML
	private void addIssuesRow() {

		int rows = issuesGridPane.getRowConstraints().size();

		issuesGridPane.getRowConstraints().add(rows - 1, new RowConstraints());

		ChoiceBox<BodyPart> bodyPartChoiceBox = new ChoiceBox<>(FXCollections.observableArrayList(BodyPart.values()));
		GridPane.setHalignment(bodyPartChoiceBox, CENTER);
		GridPane.setMargin(bodyPartChoiceBox, MARING);

		ChoiceBox<IssueType> issueTypeChoiceBox = new ChoiceBox<>(FXCollections.observableArrayList(IssueType.values()));
		GridPane.setHalignment(issueTypeChoiceBox, CENTER);
		GridPane.setMargin(issueTypeChoiceBox, new Insets(15));

		DatePicker datePicker = new DatePicker();
		GridPane.setHalignment(datePicker, CENTER);
		GridPane.setMargin(datePicker, new Insets(15));

		issuesGridPane.add(bodyPartChoiceBox, 0, rows);
		issuesGridPane.add(issueTypeChoiceBox, 1, rows);
		issuesGridPane.add(datePicker, 2, rows);
	}
}
