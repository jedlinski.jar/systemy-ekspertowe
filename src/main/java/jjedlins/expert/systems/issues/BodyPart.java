package jjedlins.expert.systems.issues;

public enum BodyPart {

	WRITS("nadgarstek"),
	ARM("ramię"),
	BARK("bark"),
	FOOT("stopa"),
	KNEE("kolano");

	private String label;

	BodyPart(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}


	@Override
	public String toString() {
		return label;
	}
}
