package jjedlins.expert.systems.issues;

public enum IssueType {

	ZLAMANIE("złamanie"),
	ZWICHNIECIE("zwichniecie"),
	SKRECENIE("skręcenie"),
	NADWYREZENIE("nadwyrezenie");

	private String label;

	IssueType(String label) {
		this.label = label;
	}


	@Override
	public String toString() {
		return label;
	}
}

