package jjedlins.expert.systems.issues;


import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import org.springframework.cglib.core.Local;

import java.time.LocalDate;

public class TableRow {


	private SimpleObjectProperty<BodyPart> bodyPartChoiceBox;
	private SimpleObjectProperty<IssueType> issueTypeChoiceBox;
	private SimpleObjectProperty<LocalDate> datePicker;

	public TableRow(BodyPart bodyPartChoiceBox, IssueType issueTypeChoiceBox, LocalDate datePicker) {
		this.bodyPartChoiceBox = new SimpleObjectProperty<>(bodyPartChoiceBox);
		this.issueTypeChoiceBox = new SimpleObjectProperty<>(issueTypeChoiceBox);
		this.datePicker = new SimpleObjectProperty<>(datePicker);
	}

	public BodyPart getBodyPartChoiceBox() {
		return bodyPartChoiceBox.get();
	}

	public SimpleObjectProperty<BodyPart> bodyPartChoiceBoxProperty() {
		return bodyPartChoiceBox;
	}

	public void setBodyPartChoiceBox(BodyPart bodyPartChoiceBox) {
		this.bodyPartChoiceBox.set(bodyPartChoiceBox);
	}

	public IssueType getIssueTypeChoiceBox() {
		return issueTypeChoiceBox.get();
	}

	public SimpleObjectProperty<IssueType> issueTypeChoiceBoxProperty() {
		return issueTypeChoiceBox;
	}

	public void setIssueTypeChoiceBox(IssueType issueTypeChoiceBox) {
		this.issueTypeChoiceBox.set(issueTypeChoiceBox);
	}

	public LocalDate getDatePicker() {
		return datePicker.get();
	}

	public SimpleObjectProperty<LocalDate> datePickerProperty() {
		return datePicker;
	}

	public void setDatePicker(LocalDate datePicker) {
		this.datePicker.set(datePicker);
	}
}

