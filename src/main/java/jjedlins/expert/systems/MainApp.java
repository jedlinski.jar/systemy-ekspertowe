package jjedlins.expert.systems;

import jjedlins.expert.systems.config.AppContextConfig;
import jjedlins.expert.systems.screen.ScreensContoller;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class MainApp extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppContextConfig.class);
        ScreensContoller bean = context.getBean(ScreensContoller.class);
        bean.init(stage);
        stage.setResizable(true);
        bean.loadScreen("/fxml/main_window.fxml");
        
    }

    public static void main(String[] args) {
        launch(args);
    }

}
